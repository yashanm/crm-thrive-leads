<?php

/**
 * Plugin Name: CRM Thrive
 * Plugin URI: https://www.crmthrive.com/
 * Description: Allows to generate leads and save to CRM thrive by using various contact form plugins.
 * Version: 1.6.2
 * Author: Yashan Mittal
 * Author URI: http://www.osnictech.com
 * License: GPLv2 or later
 */

/*  Copyright 2020  Yashan Mittal  (email : yashanmittal@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define( 'WSL_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'WSL_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WSL_INCLUDES_DIR', plugin_dir_path( __FILE__ ).'/includes' );
define( 'WSL_ADMIN_DIR', plugin_dir_path( __FILE__ ).'/admin' );

require_once WSL_INCLUDES_DIR . '/helpers.php';
require_once WSL_INCLUDES_DIR . '/class-wsl-api.php';
require_once WSL_INCLUDES_DIR . '/class-wsl-cf7-api.php';

if(is_admin()){
    require_once WSL_ADMIN_DIR . '/class-wsl-admin.php';
    $admin_obj = new Wsl_Admin();
}


// define the wpcf7_submit callback 
function wsl_wpcf7_submit( $instance, $result ) {
    
    if($result['status'] == 'validation_failed'){
        return false;
    }
    $submission = WPCF7_Submission::get_instance();
    
    $send_to_crm = wsl_get_cf7_post_setting('send_to_crm',$instance->id());
    if($send_to_crm != 1){
        return false;
    }
    
    $laravel_api = new Wsl_Cf7_Api($instance,$result);
    $laravel_api->set_submission_instance($submission);
    $laravel_api->call();
    return true;
}
         
// add the action 
//add_action( 'wpcf7_mail_sent', 'your_wpcf7_mail_sent_function' );
add_action( 'wpcf7_submit', 'wsl_wpcf7_submit', 10, 2 ); 

add_filter( 'wpcf7_editor_panels', 'wsl_add_panel' );

function wsl_add_panel($panels){
    $panels['leads-panel'] = array(
        'title' => 'Lead Settings',
        'callback' => 'wsl_leads_panel_callback'
    );
    return $panels;
}

function wsl_leads_panel_callback($post){
    include_once WSL_PLUGIN_DIR.'/admin/editor-panel.php';
}

add_action( 'wpcf7_after_save', 'wsl_cf7_success_page_save_contact_form' );

function wsl_cf7_success_page_save_contact_form( $contact_form ) {
    $contact_form_id = $contact_form->id();

//    if ( !isset( $_POST ) || empty( $_POST ) || !isset( $_POST['cf7-redirect-page-id'] ) ) {
//        return;
//    }
//    else {
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $_POST['cf7_wsl_editor_panel_nonce'], 'cf7_wsl_editor_panel' ) ) {
            return;
        }
        // Update the stored value
        $wsl_settings = array_map( 'intval', $_POST['wsl_settings'] );
        update_post_meta( $contact_form_id, '_wsl_settings', $wsl_settings );
//    }
}