<?php

class Wsl_Api{
    
    public $api_url = 'https://app.crmthrive.com/public/api/';
    
    private $api_key = false;
    
    public function __construct($api_key = false) {
        if($api_key){
            $this->set_api_key($api_key);
        }
    }
    
    public function set_api_key($key){
        $this->api_key = $key;
    }
    
    public function get_api_key(){
        if($this->api_key){
            return $this->api_key;
        }else{
            $settings = wsl_get_settings();
            if(isset($settings['api_key']) && $settings['api_key'] != ''){
                return $settings['api_key'];
            }
        }
    }
    
    public function is_api_key(){
        return '';
    }
    
    public function get_base_url(){
        return $this->api_url;
    }
    
    public function get_companies(){
        $url = $this->get_base_url().'user/companies';
        $response = $this->call($url,array(),'GET');
        return $response;
    }
    
    public function call($url, $post_data = array(), $method = 'POST'){
        $headers = array();
        $headers['Accept'] = "application/json";
        $headers['Content-Type'] = "application/json";
        $headers['Thrive-Api-Key'] = $this->get_api_key();
        
        $args = array(
            'headers' => $headers,
            'timeout' => '30',
            'redirection' => '5',
            'httpversion' => '1.0',
            'blocking' => true,
        );
        
        if($method == 'GET'){
            $response = wp_remote_get( $url,  $args);
        }elseif($method == 'POST'){
            $args['body'] = json_encode($post_data);
            $response = wp_remote_post( $url, $args );
        }else{
            $args['method'] = $method;
            $response = wp_remote_request( $url, $args );
        }
        
        $body = wp_remote_retrieve_body( $response );
        return json_decode($body);
    }
    
}

