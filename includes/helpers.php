<?php

function wsl_get_default_cf7_post_settings(){
    $wsl_settings = wsl_get_settings();
    if(is_array($wsl_settings)){
        return $wsl_settings;
    }else{
        return array(
            'send_to_crm' => 1,
            'sent_to_company' => 0
        );
    }
}

function wsl_get_cf7_post_settings($post_id){
    $meta = get_post_meta($post_id,'_wsl_settings',TRUE);
    if(!is_array($meta)){
        return wsl_get_default_cf7_post_settings();
    }
    return array_merge(wsl_get_default_cf7_post_settings(),$meta);
}

function wsl_get_cf7_post_setting($name,$post_id){
    $meta = wsl_get_cf7_post_settings($post_id);
    if(isset($meta[$name])){
        return $meta[$name];
    }else{
        return false;
    }
}

function wsl_get_default_settings(){
    $default = array(
        'api_key' => '',
        'send_to_crm' => 1,
        'send_to_company' => 0,
    );
    return $default;
}

function wsl_get_settings(){
    $settings = get_option('wsl_settings',wsl_get_default_settings());
    return $settings;
}

function wsl_update_setting($name,$value){
    $settings = get_option('wsl_settings',wsl_get_default_settings());
    $settings[$name] = $value;
    update_option('wsl_settings', $settings);
}

function wsl_get_setting($name){
    $settings = get_option('wsl_settings',wsl_get_default_settings());
    return isset($settings[$name])?$settings[$name] : false;
}

function wsl_check_default_company_exists($companies){
    $company_websites = array_column(array_map('get_object_vars', $companies), 'website','id');
    $id = array_search(site_url(), $company_websites);
    if($id){
        return $id;
    }else{
        return 0;
    }
}

