<?php if($companies->type != 'error'): ?>
<?php 
global $pagenow;
?>
<fieldset>
    <p>
        <select name="wsl_settings[send_to_company]">
            <?php 
                if(count($companies->data) > 0):
                    $company_ids = array_column(array_map('get_object_vars', $companies->data), 'id');
                    ?>
            <?php if($pagenow == 'options-general.php'): ?>
            <option <?php selected(!in_array($meta['send_to_company'], $company_ids),true,true) ?> value="<?php wsl_check_default_company_exists($companies->data) ?>"><?php echo get_bloginfo('name'); ?> (Created by Default)</option>
                <?php else: ?>
            <option <?php selected(!in_array($meta['send_to_company'], $company_ids),true,true) ?> value="0">Default Plugin Settings</option>
                <?php endif; ?>    
                <?php
                    foreach($companies->data as $company): 
                        if($company->website == site_url()){
                            continue;
                        }
                    ?>
                        <option <?php selected($meta['send_to_company'], $company->id, true); ?> value="<?php echo $company->id ?>"><?php echo $company->name; ?></option>
            <?php 
                    endforeach; 
                else:
            ?>
                    <option value="0"><?php echo get_bloginfo('name'); ?> (Will be created by Default)</option>    
            <?php endif; ?>
        </select>
    </p>
    <p class="description">Select a company to send the Leads to in the CRM</p>
</fieldset>
<?php else: ?>
<fieldset>
    <p>
        <select>
            <option value="0">Companies Not Available</option>
        </select>
    </p>
    <p class="description">Please add valid Api key to select the company from your CRM account.</p>
</fieldset>
<?php endif; ?>